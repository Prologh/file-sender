﻿# File Sender

README / 07 June 2019

-----

## Introduction

File Sender is a Windows desktop application designed for transferring files over TCP protocol.

![](https://i.imgur.com/c3PW4vF.png)

## Technical information

Name | Value
--- | ---
Main programming language | C#
Framework used | .NET Framework 4.7

## Getting started

Download the [1.0 version](https://gitlab.com/Prologh/file-sender/tags/v1.0) from GitLab or clone the project and build it on your own.

## How to use

### Recieving

 1. Launch the app and choose the *Server* tab;
 2. Start the server by clicking the *Start* button. IP address and port number are determined automatically, but you can always set them manually;
 3. That's it! Now, let's send some files!

###### Note: recieved files will be automatically saved on current user desktop.

### Sending

 1. Launch the app and choose the *Client* tab;
 2. Tell the app which file you would like to send. Click the *Browse* button and choose a file;
 3. Change the IP address and port number to corresponding remote server values;
 3. Click the *Send* button.
 4. That's it! The file should be sent. Application should generate similar output:

![](https://i.imgur.com/6wcP8L4.png)

## License

Licensed under MIT. Read full license [here](https://gitlab.com/Prologh/file-sender/blob/master/LICENSE).

## Credits

**Piotr Wosiek** | [GitLab](https://gitlab.com/Prologh) | [GitHub](https://github.com/Prologh)

### Acknowledgements

Project icon based on one made by [Freepik](https://www.freepik.com/) from [www.flaticon.com](https://www.flaticon.com/).