﻿namespace FileSender.DesktopApp.Data
{
    /// <summary>
    /// Contains all static and constant values used in application.
    /// </summary>
    public static class Constants
    {
        /// <summary>
        /// Contains error messages.
        /// </summary>
        public static class Errors
        {
            /// <summary>
            /// All aplication ports are currently occupied.
            /// </summary>
            public static string AllAplicationPortsOccupied =
                "All aplication ports are occupied. Please, ensure that other " +
                "applications are not blocking the network.";

            /// <summary>
            /// Selected file does not existing or has been removed.
            /// </summary>
            public static string FileNotExist =
                "Selected file does not exists or has been deleted!";

            /// <summary>
            /// No file selected by user.
            /// </summary>
            public static string NoFileSelected = "No file selected";
        }

        /// <summary>
        /// Contains layout related values.
        /// </summary>
        public static class Layout
        {
            /// <summary>
            /// Default file name.
            /// </summary>
            public static string DefaultFileName = string.Empty;

            /// <summary>
            /// Default value for progress bar.
            /// </summary>
            public static int DefaultProgressBarValue = 0;
        }

        /// <summary>
        /// Contains networking related values.
        /// </summary>
        public static class Net
        {
            /// <summary>
            /// Timeout for sending and receiving data in miliseconds.
            /// </summary>
            public static int Timeout = 8000;

            /// <summary>
            /// Contains file buffer related values.
            /// </summary>
            public static class FileBuffers
            {
                /// <summary>
                /// Maximum size of file length expressed as size of byte array.
                /// </summary>
                public static int FileLengthSize = sizeof(long); // 8 bytes

                /// <summary>
                /// File name buffer size.
                /// </summary>
                public static int FileNameSize = 512;

                /// <summary>
                /// Default buffer size.
                /// </summary>
                public static int DefaultBufferSize = 1024;
            }

            /// <summary>
            /// Contains network port numbers.
            /// </summary>
            public static class Ports
            {
                /// <summary>
                /// Maximum value in a range of application used ports.
                /// </summary>
                public static int ApplicationMax = 8130;

                /// <summary>
                /// Minimum value in a range of application used ports.
                /// </summary>
                public static int ApplicationMin = 8080;

                /// <summary>
                /// Default port number.
                /// </summary>
                public static int Default = 8080;

                /// <summary>
                /// Maximum valid user (registered) port number.
                /// </summary>
                public static int UserMax = 49151;

                /// <summary>
                /// Minimum valid user (registered) port number.
                /// </summary>
                public static int UserMin = 1024;
            }
        }
    }
}
