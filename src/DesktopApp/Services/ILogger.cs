﻿namespace FileSender.DesktopApp.Services
{
    /// <summary>
    /// Represents a type used to perform logging.
    /// </summary>
    public interface ILogger
    {
        /// <summary>
        /// Gets a <see cref="bool"/> value indicating whether current logging
        /// service is enabled, this is, can log messages.
        /// </summary>
        bool IsEnabled { get; }

        /// <summary>
        /// Removes all logs.
        /// </summary>
        void ClearLogger();

        /// <summary>
        /// Appends log message.
        /// </summary>
        /// <param name="message">
        /// The log message.
        /// </param>
        void Log(string message);

        /// <summary>
        /// Appends log entry to logger output.
        /// </summary>
        /// <param name="message">
        /// The log message.
        /// </param>
        /// <param name="level">
        /// Log entry level.
        /// </param>
        void Log(string message, LogLevel level);
    }
}
