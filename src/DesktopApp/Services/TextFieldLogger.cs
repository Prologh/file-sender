﻿using System;
using System.Windows.Controls.Primitives;

namespace FileSender.DesktopApp.Services
{
    /// <summary>
    /// Represents a type used to perform logging inside a text box.
    /// </summary>
    public class TextFieldLogger : ILogger
    {
        private readonly TextBoxBase loggingTextBox;

        /// <summary>
        /// Initializes a new instance of the <see cref="TextFieldLogger"/> class.
        /// </summary>
        /// <param name="loggingTextBox">
        /// The <see cref="TextBoxBase"/>
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="loggingTextBox"/> is <see langword="null"/>.
        /// </exception>
        public TextFieldLogger(TextBoxBase loggingTextBox)
        {
            this.loggingTextBox = loggingTextBox ?? throw new ArgumentNullException(nameof(loggingTextBox));
        }

        /// <summary>
        /// Gets a <see cref="bool"/> value indicating whether current logging
        /// service is enabled, this is, can log messages.
        /// </summary>
        public bool IsEnabled { get; }

        /// <summary>
        /// Removes all logs.
        /// </summary>
        public void ClearLogger()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Appends log message.
        /// </summary>
        /// <param name="message">
        /// The log message.
        /// </param>
        public void Log(string message)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Appends log entry to logger output.
        /// </summary>
        /// <param name="message">
        /// The log message.
        /// </param>
        /// <param name="level">
        /// Log entry level.
        /// </param>
        public void Log(string message, LogLevel level)
        {
            throw new NotImplementedException();
        }
    }
}
