﻿namespace FileSender.DesktopApp.Net
{
    using FileSender.DesktopApp.Data;
    using FileSender.DesktopApp.Views;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net;
    using System.Net.Sockets;
    using System.Text;
    using System.Threading;

    /// <summary>
    /// Wrapper over <see cref="TcpListener"/> providing server-like
    /// functionalities.
    /// </summary>
    public class TcpServer
    {
        private readonly IList<TcpServerClient> _clients;
        private readonly CancellationTokenSource _listenerTokenSource;
        private readonly CancellationToken _listenerToken;
        private readonly TcpListenerWrapper _server;

        /// <summary>
        /// Initializes a new instance of the <see cref="TcpServer"/> class
        /// from <see cref="System.Net.IPAddress"/> and port.
        /// </summary>
        /// <param name="ipAddress">
        /// IP address on which the underlaying listener should be established.
        /// </param>
        /// <param name="port">
        /// Port number on which the underlaying listener should be listening.
        /// </param>
        public TcpServer(IPAddress ipAddress, int port)
        {
            _clients = new List<TcpServerClient>();
            _listenerTokenSource = new CancellationTokenSource();
            _listenerToken = _listenerTokenSource.Token;
            _server = new TcpListenerWrapper(ipAddress, port);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TcpServer"/> class
        /// from <see cref="IPEndPoint"/>.
        /// </summary>
        /// <param name="ipEndPoint">
        /// IP end point on which the underlaying listener should be established.
        /// </param>
        public TcpServer(IPEndPoint ipEndPoint)
        {
            _clients = new List<TcpServerClient>();
            _listenerTokenSource = new CancellationTokenSource();
            _listenerToken = _listenerTokenSource.Token;
            _server = new TcpListenerWrapper(ipEndPoint);
        }

        /// <summary>
        /// Gets or sets the file buffer size which server is using.
        /// </summary>
        public int FileBufferSize => Constants.Net.FileBuffers.DefaultBufferSize;

        /// <summary>
        /// Gets the amount of currently connected clients.
        /// </summary>
        public int ClientsCount => _clients.Count;

        /// <summary>
        /// Indicates whether server has any clients currently connected.
        /// </summary>
        public bool HasClients => ClientsCount > 0;

        /// <summary>
        /// Gets the local <see cref="System.Net.IPAddress"/> on which
        /// <see cref="TcpServer"/> is established.
        /// </summary>
        public IPAddress IPAddress => LocalIPEndPoint.Address;

        /// <summary>
        /// Indicates whether <see cref="TcpServer"/> is actively listening
        /// for client connections.
        /// </summary>
        public bool IsListening => _server.IsListening;

        /// <summary>
        /// Gets the underlying <see cref="IPEndPoint"/> of the current
        /// <see cref="TcpListener"/>.
        /// </summary>
        public IPEndPoint LocalIPEndPoint => (IPEndPoint)_server.LocalEndpoint;

        /// <summary>
        /// Gets the port number on which underlying <see cref="Socket"/>
        /// is listening.
        /// </summary>
        public int Port => LocalIPEndPoint.Port;

        /// <summary>
        /// Shuts down the <see cref="TcpServer"/>, closes the
        /// <see cref="TcpListener"/>, terminates all connections and releases
        /// all associated resources.
        /// </summary>
        public void Shutdown()
        {
            TerminateConnections();

            if (IsListening)
            {
                _server.Stop();
            }

            _server.Server.Close();
            _listenerTokenSource.Cancel();
        }

        /// <summary>
        /// Starts tcp server and launches the listener.
        /// </summary>
        public void StartServer()
        {
            _server.Start();
            _server.BeginAcceptTcpClient(new AsyncCallback(OnClientConnected), null);
        }

        /* Private methods */

        private async void HandleClient(TcpServerClient tcpServerClient)
        {
            if (tcpServerClient == null)
            {
                throw new ArgumentNullException(nameof(tcpServerClient));
            }

            if (tcpServerClient.TcpClient == null)
            {
                throw new NullReferenceException(nameof(tcpServerClient.TcpClient));
            }

            var clientPrefix = $"[{tcpServerClient.GetFullLocalAddress()}]";
            ClientView.AppendLog($"{clientPrefix} Connection opened.");

            try
            {
                if (!tcpServerClient.IsCancellationRequested
                    && IsListening
                    && _server.Server.IsBound
                    && tcpServerClient.TcpClient.Connected)
                {

                    // Read the name of the file.
                    var fileNameBuffer = new byte[Constants.Net.FileBuffers.FileNameSize];
                    var writer = tcpServerClient.TcpClient.GetStream();
                    await writer.ReadAsync(fileNameBuffer, 0, fileNameBuffer.Length, tcpServerClient.Token);
                    var fileName = Encoding.Unicode.GetString(fileNameBuffer).Trim('\0');

                    // Read the length of file.
                    var fileLengthBuffer = new byte[Constants.Net.FileBuffers.FileLengthSize];
                    await writer.ReadAsync(fileLengthBuffer, 0, fileLengthBuffer.Length, tcpServerClient.Token);
                    var fileLength = BitConverter.ToInt64(fileLengthBuffer, 0);
                    var packages = (int)(fileLength / FileBufferSize);
                    if (fileLength % FileBufferSize != 0)
                    {
                        packages++;
                    }

                    // Read file packages, open file to write.
                    var bufferSizeEnding = FileBufferSize;
                    var targetDirectory = GetSpecialFolderDirectory(Environment.SpecialFolder.Desktop);
                    var filePath = $"{targetDirectory}\\{fileName}";

                    ServerView.AppendLog($"{clientPrefix} Receiving file.");

                    using (var file = File.OpenWrite(filePath))
                    {
                        using (var memoryStream = new MemoryStream())
                        {
                            for (var packageNum = 0; packageNum < packages; packageNum++)
                            {
                                if (tcpServerClient.IsCancellationRequested)
                                {
                                    memoryStream.Dispose();
                                    file.Close();
                                    throw new OperationCanceledException();
                                }

                                if (!File.Exists(filePath))
                                {
                                    throw new Exception(
                                        $"{clientPrefix} " +
                                        "Opened file does not exist, " +
                                        "cannot be accessed " +
                                        "or has been deleted!");
                                }

                                // Calculate package size if it is the last one.
                                if (fileLength % FileBufferSize != 0 && packageNum + 1 >= packages)
                                {
                                    bufferSizeEnding = (int)(fileLength - FileBufferSize * (packages - 1));
                                }

                                // Read file package.
                                var buffer = new byte[bufferSizeEnding];
                                await writer.ReadAsync(
                                    buffer,
                                    0,
                                    buffer.Length,
                                    tcpServerClient.Token);

                                // Write file package to file.
                                await file.WriteAsync(
                                    buffer,
                                    0,
                                    buffer.Length,
                                    tcpServerClient.Token);
                            }
                        }

                        ServerView.AppendLog(
                            $"{clientPrefix} Received {fileLength} bytes " +
                            $"in {packages} packages.");
                        ServerView.AppendLog($"{clientPrefix} File received successfully.");
                    }
                }

                ServerView.AppendLog($"{clientPrefix} Connection closed successfully.");
            }
            catch (OperationCanceledException)
            {
                ServerView.AppendLog($"{clientPrefix} Connection closed by user.");
            }
            catch (Exception)
            {
                ServerView.AppendLog($"{clientPrefix} Connection closed with error/s.");
            }
            finally
            {
                tcpServerClient.TcpClient.Close();
            }
        }

        private void OnClientConnected(IAsyncResult asyncResult)
        {
            try
            {
                var tcpClient = _server.EndAcceptTcpClient(asyncResult);
                var tcpServerClient = new TcpServerClient(tcpClient);

                HandleClient(tcpServerClient);
            }
            catch
            {
                if (IsListening)
                {
                    throw;
                }
            }

            if (IsListening)
            {
                _server.BeginAcceptTcpClient(new AsyncCallback(OnClientConnected), null);
            }
        }

        private void TerminateConnections()
        {
            for (int i = 0; i < ClientsCount; i++)
            {
                var client = _clients[i];
                client.TcpClient.Close();
                //client.TokenSource.Cancel();
            }
        }

        /* Public static methods */

        /// <summary>Gets the local IP address.</summary>
        /// <returns>Local IP address represented as <see cref="string"/>.</returns>
        public static string GetLocalIPAddress()
        {
            var localIP = string.Empty;
            try
            {
                var host = Dns.GetHostEntry(Dns.GetHostName());

                foreach (var ip in host.AddressList)
                {
                    if (ip.AddressFamily == AddressFamily.InterNetwork)
                    {
                        localIP = ip.ToString();
                        break;
                    }
                }
            }
            catch (Exception) { }

            return localIP;
        }

        /// <summary>
        /// Gets the path for special folder.
        /// </summary>
        /// <param name="directoryType">
        /// Special folder type represented as <see cref="Environment.SpecialFolder"/>.
        /// </param>
        /// <returns>
        /// Path to the requested special folder represented as a <see cref="string"/> value.
        /// </returns>
        public static string GetSpecialFolderDirectory(Environment.SpecialFolder directoryType)
        {
            return Environment.GetFolderPath(directoryType);
        }
    }
}
