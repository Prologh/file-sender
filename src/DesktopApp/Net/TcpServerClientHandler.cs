﻿namespace FileSender.DesktopApp.Net
{
    using FileSender.DesktopApp.Data;
    using FileSender.DesktopApp.Views;
    using System;
    using System.IO;
    using System.Net;
    using System.Net.Sockets;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;

    public class TcpServerClientHandler
    {
        private TcpClient _client;
        private CancellationTokenSource _tokenSource;

        public TcpServerClientHandler(IPAddress ipAddress, int port)
        {
            // Populate properties.
            _tokenSource = new CancellationTokenSource();
            IPAddress = ipAddress;
            Port = port;

            try
            {
                #region getting-random-port
                var attempt = 0;
                var maxAttempts = Constants.Net.Ports.ApplicationMax - Constants.Net.Ports.ApplicationMin;
                bool portBlocked;
                do
                {
                    if (++attempt > maxAttempts)
                        throw new Exception(Constants.Errors.AllAplicationPortsOccupied);

                    portBlocked = false;
                    var randomPort = new Random().Next(Constants.Net.Ports.UserMin, Constants.Net.Ports.UserMax + 1);

                    // Create a local endpoint.
                    ClientEndPoint = new IPEndPoint(IPAddress.Any, randomPort);
                    try
                    {
                        // Create a TCP Client.
                        _client = new TcpClient(ClientEndPoint);
                    }
                    catch (SocketException)
                    {
                        portBlocked = true;
                    }
                } while (portBlocked);

                ClientView.AppendLog($"Connecting through port {ClientEndPoint.Port}.");
                #endregion
            }
            catch (Exception ex)
            {
                Disconnect();
                throw ex;
            }
        }

        public bool IsConnected { get; private set; }

        public IPEndPoint ClientEndPoint { get; private set; }

        public IPAddress IPAddress { get; private set; }

        public int Port { get; private set; }

        public int BufferSize { get; private set; } = Constants.Net.FileBuffers.DefaultBufferSize;

        public async Task ConnectAsync()
        {
            _client.ReceiveTimeout = Constants.Net.Timeout;
            try
            {
                // Connect the client to the remote server.
                await Task.Run(() =>
                {
                    _client.ConnectAsync(IPAddress, Port);
                }, _tokenSource.Token);
            }
            catch (Exception ex)
            {
                Disconnect();
                throw ex;
            }
            IsConnected = true;
        }

        public void Disconnect()
        {
            if (IsConnected)
            {
                _tokenSource.Cancel();
                try
                {
                    _client.GetStream().Close();
                    _client.Close();
                }
                catch (Exception) { }
                finally
                {
                    _client = null;
                    IsConnected = false;
                }
            }
            else
            {
                this._client = null;
            }
        }

        public async Task<bool> SendFileAsync(string filePath, string fileName)
        {
            var packageNum = 0;
            try
            {
                if (!_tokenSource.IsCancellationRequested
                    && IsConnected)
                {
                    //## (1) Send the file name.
                    var fileNameBuffer = Encoding.Unicode.GetBytes(fileName);
                    if (fileNameBuffer.Length > Constants.Net.FileBuffers.FileNameSize)
                    {
                        throw new Exception("File name too long!");
                    }
                    var writer = _client.GetStream();
                    await writer.WriteAsync(
                        fileNameBuffer,
                        0,
                        fileNameBuffer.Length,
                        _tokenSource.Token);


                    // Flush the stream.
                    await Task.Run(() =>
                        writer.FlushAsync(),
                        _tokenSource.Token);

                    var packages = 0;
                    var fileLength = 0L;
                    var BufferSizeEnding = BufferSize;
                    using (var file = File.OpenRead(filePath))
                    {
                        //## (2) Send the length of file.
                        fileLength = file.Length;
                        packages = (int)(fileLength / BufferSize);
                        if (fileLength % BufferSize != 0)
                            packages++;

                        ClientView.SetProgressValues(0, packages);
                        var fileLengthBuffer = BitConverter.GetBytes((int)fileLength);
                        if (fileLengthBuffer.Length > Constants.Net.FileBuffers.FileLengthSize)
                        {
                            throw new Exception("File too big!");
                        }

                        await writer.WriteAsync(
                            fileLengthBuffer,
                            0,
                            fileLengthBuffer.Length,
                            _tokenSource.Token);

                        // Flush the stream.
                        await Task.Run(() =>
                            writer.FlushAsync(),
                            _tokenSource.Token);

                        using (var ms = new MemoryStream())
                        {
                            for (packageNum = 0; packageNum < packages; packageNum++)
                            {
                                if (_tokenSource.IsCancellationRequested)
                                {
                                    ms.Dispose();
                                    file.Close();
                                    throw new OperationCanceledException();
                                }

                                if (!File.Exists(filePath))
                                    throw new Exception(Constants.Errors.NoFileSelected);

                                if (fileLength % BufferSize != 0
                                    && packageNum + 1 >= packages)
                                    BufferSizeEnding = (int)(fileLength - BufferSize * (packages - 1));

                                // Read file package.
                                var buffer = new byte[BufferSizeEnding];
                                await file.ReadAsync(
                                    buffer,
                                    0,
                                    buffer.Length,
                                    _tokenSource.Token);

                                // Send file package.
                                await writer.WriteAsync(
                                    buffer,
                                    0,
                                    buffer.Length,
                                    _tokenSource.Token);

                                // Flush the stream.
                                await Task.Run(() =>
                                    writer.FlushAsync(),
                                    _tokenSource.Token);

                                ClientView.AppendProgress();
                            }
                        }
                        file.Close();
                    }
                    WriteFileLengthStats(packages, fileLength);
                }
                _client.GetStream().Close();
                _client.Close();
                IsConnected = false;

                return true;
            }
            catch (OperationCanceledException)
            {
                return false;
            }
            catch (Exception ex)
            {
                Disconnect();
                throw ex;
            }
        }

        private void WriteFileLengthStats(int packagesAmount, long fileLength)
        {
            ClientView.AppendLog(
                $"Sent {fileLength} bytes" +
                $" in {packagesAmount} packages.");
        }

        /* Public static methods */

        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }

            return "_._._._";
        }
    }
}
