﻿namespace FileSender.DesktopApp.Net
{
    using System;
    using System.Net;
    using System.Net.Sockets;

    /// <summary>
    /// Represents wrapper over <see cref="TcpListener"/>.
    /// </summary>
    public class TcpListenerWrapper : TcpListener
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TcpListenerWrapper"/> class
        /// with the specified local endpoint.
        /// </summary>
        /// <param name="localEP">
        /// An <see cref="IPEndPoint"/> that represents the local endpoint
        /// to which to bind the listener <see cref="Socket"/>.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="localEP"/> is <see langword="null"/>.
        /// </exception>
        public TcpListenerWrapper(IPEndPoint localEP) : base(localEP)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TcpListenerWrapper"/>
        /// class that listens for incoming connection attempts on the specified local IP address and port number.
        /// </summary>
        /// <param name="localaddr">
        /// An <see cref="IPAddress"/> that represents the local IP address.
        /// </param>
        /// <param name="port">
        /// The port on which to listen for incoming connection attempts.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="localaddr"/> is <see langword="null"/>.
        /// </exception>
        /// <exception cref="ArgumentOutOfRangeException">
        /// <paramref name="port"/> is not between
        /// <see cref="IPEndPoint.MinPort"/> and <see cref="IPEndPoint.MaxPort"/>.
        /// </exception>
        public TcpListenerWrapper(IPAddress localaddr, int port) : base(localaddr, port)
        {

        }

        /// <summary>
        /// Gets a <see cref="bool"/> value that indicates whether
        /// <see cref="TcpListener"/> is actively listening for client
        /// connections.
        /// </summary>
        public bool IsListening => Active;
    }
}
