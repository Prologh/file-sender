﻿namespace FileSender.DesktopApp.Net
{
    using System.Diagnostics;
    using System.Net;
    using System.Net.Sockets;
    using System.Threading;

    /// <summary>
    /// Represents enhanced client for TCP network connections.
    /// </summary>
    [DebuggerDisplay("{LocalIPv4,nq}:{RemotePort,nq}")]
    public class TcpServerClient
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TcpServerClient"/>
        /// class on base of <see cref="System.Net.Sockets.TcpClient"/>.
        /// </summary>
        /// <param name="client"><see cref="System.Net.Sockets.TcpClient"/> instance.</param>
        public TcpServerClient(TcpClient client)
        {
            TcpClient = client;
            TokenSource = new CancellationTokenSource();
        }

        /// <summary>
        /// Gets the value indicating whether cancellation of <see cref="CancellationToken"/>
        /// for this <see cref="TcpServerClient"/> has been requested.
        /// </summary>
        public bool IsCancellationRequested => Token.IsCancellationRequested;

        /// <summary>
        /// Gets the client's end point.
        /// </summary>
        public EndPoint LocalEndPoint => TcpClient.Client.LocalEndPoint;

        /// <summary>
        /// Gets the <see cref="IPEndPoint"/> for local connection.
        /// </summary>
        public IPEndPoint LocalIPEndPoint => LocalEndPoint as IPEndPoint;

        /// <summary>
        /// Gets the client's locacl <see cref="IPAddress"/> mapped to
        /// Internet Protocol version 4.
        /// </summary>
        public IPAddress LocalIPv4 => LocalIPEndPoint.Address.MapToIPv4();

        /// <summary>
        /// Gets the client's local <see cref="IPAddress"/> mapped to
        /// Internet Protocol version 6.
        /// </summary>
        public IPAddress LocalIPv6 => LocalIPEndPoint.Address.MapToIPv6();

        /// <summary>
        /// Gets the client's port of local connection.
        /// </summary>
        public int LocalPort => LocalIPEndPoint.Port;

        /// <summary>
        /// Gets the client's end point.
        /// </summary>
        public EndPoint RemoteEndPoint => TcpClient.Client.RemoteEndPoint;

        /// <summary>
        /// Gets the <see cref="IPEndPoint"/> for remote connection.
        /// </summary>
        public IPEndPoint RemoteIPEndPoint => RemoteEndPoint as IPEndPoint;

        /// <summary>
        /// Gets the client's remote <see cref="IPAddress"/> mapped to
        /// Internet Protocol version 4.
        /// </summary>
        public IPAddress RemoteIPv4 => RemoteIPEndPoint.Address.MapToIPv4();

        /// <summary>
        /// Gets the client's remote <see cref="IPAddress"/> mapped to
        /// Internet Protocol version 6.
        /// </summary>
        public IPAddress RemoteIPv6 => RemoteIPEndPoint.Address.MapToIPv6();

        /// <summary>
        /// Gets the client's port of remote connection.
        /// </summary>
        public int RemotePort => RemoteIPEndPoint.Port;

        /// <summary>
        /// Gets the underlying <see cref="System.Net.Sockets.TcpClient"/> instance.
        /// </summary>
        public TcpClient TcpClient { get; }

        /// <summary>
        /// Gets the <see cref="CancellationToken"/> of Client.
        /// </summary>
        public CancellationToken Token => TokenSource.Token;

        /// <summary>
        /// Gets the <see cref="CancellationTokenSource"/> of Client.
        /// Call <see cref="CancellationTokenSource.Cancel"/> to terminate
        /// this client's connection to server.
        /// </summary>
        public CancellationTokenSource TokenSource { get; }

        /// <summary>
        /// Gets the full address (IP and port) of locally connected underlying
        /// <see cref="Socket"/>.
        /// </summary>
        /// <returns>
        /// Full IP address and port number represented as <see cref="string"/>
        /// value.
        /// </returns>
        public string GetFullLocalAddress()
        {
            return $"{LocalIPv4}:{RemotePort}";
        }

        /// <summary>
        /// Gets the full address (IP and port) of remotely connected underlying
        /// <see cref="Socket"/>.
        /// </summary>
        /// <returns>
        /// Full IP address and port number represented as <see cref="string"/>
        /// value.
        /// </returns>
        public string GetFullRemoteAddress()
        {
            return $"{RemoteIPv4}:{RemotePort}";
        }
    }
}
