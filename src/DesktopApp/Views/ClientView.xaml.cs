﻿using FileSender.DesktopApp.Data;
using FileSender.DesktopApp.Net;
using FileSender.DesktopApp.ViewModels;
using Microsoft.Win32;
using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

namespace FileSender.DesktopApp.Views
{
    /// <summary>
    /// Interaction logic for ClientTab.xaml
    /// </summary>
    public partial class ClientView : UserControl
    {
        private static TextBox _logger;
        private static ProgressBar _progressBar;

        private string _filePath;
        private bool _isFileSelected;
        private bool _isConnected;
        private bool _isConnecting;
        private TcpServerClientHandler _client;
        private ClientVM _dataContext;

        public ClientView()
        {
            InitializeComponent();
            BindDataContext();
            _logger = _textBoxLog;
            _progressBar = _progressBarUpload;
        }

        private void BindDataContext()
        {
            _dataContext = GetInitialBingindData();
            DataContext = _dataContext;
        }

        private ClientVM GetInitialBingindData()
        {
            return new ClientVM()
            {
                IPAddress = TcpServerClientHandler.GetLocalIPAddress(),
                Port = Constants.Net.Ports.Default.ToString(),
                FileName = Constants.Layout.DefaultFileName,
                Progress = Constants.Layout.DefaultProgressBarValue,
            };
        }

        private async void OpenConnection()
        {
            if (_isConnected) return;
            ClearProgressBar();

            if (!IPAddress.TryParse(_dataContext.IPAddress, out IPAddress ip))
            {
                AppendLog("IP address formatted not properly.");
                return;
            }

            if (!int.TryParse(_dataContext.Port, out int port)
                || port < IPEndPoint.MinPort
                || port > IPEndPoint.MaxPort)
            {
                AppendLog("Port value is not correct!");
                return;
            }

            if (!_isFileSelected)
            {
                AppendLog("No file selected! Please select file first.");
                return;
            }

            if (!File.Exists(_filePath))
            {
                AppendLog(Constants.Errors.FileNotExist);
                return;
            }

            _isConnecting = true;
            AppendLog($"Connecting to {_dataContext.IPAddress}:{port}.");
            try
            {
                _client = new TcpServerClientHandler(ip, port);
                await _client.ConnectAsync();
            }
            catch (Exception ex)
            {
                AppendLog("Unable to connect. Error details:");
                AppendLog(ex.Message);
                Disconnect();
                return;
            }
            _isConnecting = false;
            _isConnected = true;
            ToggleSendButtonContent();
            AppendLog("Connected.");

            await SendFileAsync(_filePath, _dataContext.FileName);
        }

        private async Task SendFileAsync(string filePath, string fileSafeName)
        {
            AppendLog("Sending file.");
            //_uploadLock = true;
            bool uploadSuccessful;
            try
            {
                uploadSuccessful = await _client.SendFileAsync(
                    filePath, fileSafeName);
            }
            catch (Exception ex)
            {
                AppendLog("Unable to send file. Error details:");
                AppendLog(ex.Message);
                Disconnect();
                return;
            }

            if (uploadSuccessful)
            {
                AppendLog("File sent successfully.");
            }

            Disconnect(clearProgress: false);
        }

        private void Disconnect(bool clearProgress = true)
        {
            _isConnecting = false;
            if (!_isConnected)
            {
                return;
            }

            _client.Disconnect();
            _client = null;
            if (clearProgress)
            {
                ClearProgressBar();
            }

            _isConnected = false;
            ToggleSendButtonContent();
            AppendLog("Disconnected.");
        }

        private void ButtonSend_Click(object sender, RoutedEventArgs e)
        {
            if (_isConnected)
            {
                AppendLog("Connection cancelled by user.\n");
                Disconnect(false);
            }
            else
            {
                OpenConnection();
            }
        }

        private void ButtonBrowseFile_Click(object sender, RoutedEventArgs e)
        {
            var fileDialog = new OpenFileDialog()
            {
                CheckFileExists = true,
                CheckPathExists = true,
                Multiselect = false,
                ValidateNames = true,
                Title = "Select file",
            };

            if (fileDialog.ShowDialog() == true)
            {
                _isFileSelected = true;
                _filePath = fileDialog.FileName;
                _dataContext.FileName = fileDialog.SafeFileName;
            }
        }

        private void ButtonReset_Click(object sender, RoutedEventArgs e)
        {
            Disconnect();
            ClearFileCache();
            ClearProgressBar();
            BindDataContext();
            _textBoxLog.Clear();
        }

        private void ButtonExit_Click(object sender, RoutedEventArgs e)
        {
            Disconnect();
            Application.Current.Shutdown();
        }

        private void TextBoxLog_TextChanged(object sender, TextChangedEventArgs e)
        {
            var textBox = (TextBox)sender;
            if (!textBox.IsKeyboardFocusWithin)
            {
                textBox.ScrollToEnd();
            }
        }

        private void ClearFileCache()
        {
            _isFileSelected = false;
            _filePath = string.Empty;
            _dataContext.FileName = Constants.Layout.DefaultFileName;
        }

        private void ClearProgressBar()
        {
            _dataContext.Progress = 0;
        }

        private void ToggleSendButtonContent()
        {
            ButtonSend.Content = (_isConnected) ? "Disconnect" : "Send";
        }

        /* Public static methods */

        public static void SetProgressValues(int minimumValue, int maximumValue)
        {
            _progressBar.Minimum = minimumValue;
            _progressBar.Maximum = maximumValue;
        }

        public static void AppendProgress()
        {
            _progressBar.Value++;
        }

        public static void AppendLog(string log, bool autoLineEnd = true)
        {
            var datePrefix = DateTime.Now.ToString("HH:mm:ss") + ": ";
            if (!log.EndsWith("\n") && autoLineEnd)
            {
                log += "\n";
            }

            Application.Current.Dispatcher.BeginInvoke(
                DispatcherPriority.Background,
                new Action(() => _logger.AppendText(datePrefix + log)));
        }
    }
}
