﻿namespace FileSender.DesktopApp.Views
{
    using FileSender.DesktopApp.Data;
    using FileSender.DesktopApp.Net;
    using FileSender.DesktopApp.ViewModels;
    using System;
    using System.Net;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;
    using System.Windows.Threading;

    /// <summary>
    /// Interaction logic for ServerTab.xaml
    /// </summary>
    public partial class ServerView : UserControl
    {
        private static TextBox _logger;

        private ServerVM _severVM;
        private TcpServer _tcpServer;

        public ServerView()
        {
            InitializeComponent();
            BindDataContext();
            _logger = _textBoxLog;
        }
        public bool IsServerRunning { get => _tcpServer != null && _tcpServer.IsListening; }

        private void BindDataContext()
        {
            _severVM = GetInitialViewData();
            DataContext = _severVM;
        }

        private ServerVM GetInitialViewData()
        {
            return new ServerVM()
            {
                IPAddress = TcpServer.GetLocalIPAddress(),
                Port = Constants.Net.Ports.Default.ToString(),
            };
        }

        private void ButtonConnect_Click(object sender, RoutedEventArgs e)
        {
            if (IsServerRunning)
            {
                StopServer();
            }
            else
            {
                StartServer();
            }
        }

        private void StartServer()
        {
            if (IsServerRunning)
            {
                return;
            }

            if (!IPAddress.TryParse(_severVM.IPAddress, out IPAddress ip))
            {
                AppendLog("IP address formatted not properly.");
                return;
            }

            if (!int.TryParse(_severVM.Port, out int port)
                || port < IPEndPoint.MinPort
                || port > IPEndPoint.MaxPort)
            {
                AppendLog("Port value is not correct!");
                return;
            }

            AppendLog($"Starting server at {_severVM.IPAddress}:{_severVM.Port}.");
            try
            {
                _tcpServer = new TcpServer(ip, port);
                _tcpServer.StartServer();
            }
            catch (Exception ex)
            {
                AppendLog("An error ocurred during server startup. Details:");
                AppendLog(ex.Message);
                StopServer();
                return;
            }

            if (IsServerRunning)
            {
                ApplyServerState();
                AppendLog("Server is running.");
            }
        }

        private void StopServer()
        {
            if (IsServerRunning)
            {
                AppendLog("Stopping server.");

                _tcpServer.Shutdown();
                ApplyServerState();

                AppendLog("Server stopped.");
            }
        }

        private void ButtonReset_Click(object sender, RoutedEventArgs e)
        {
            ResetAll();
        }

        private void ResetAll()
        {
            StopServer();
            BindDataContext();
            ClearLog();
        }

        private void ApplyServerState()
        {
            _buttonConnect.Content = (IsServerRunning) ? "Stop" : "Start";
            LabelServerState.Content = (IsServerRunning) ? "running" : "stopped";
            LabelServerState.Foreground = (IsServerRunning) ?
                new SolidColorBrush(Colors.DarkGreen)
                : new SolidColorBrush(Colors.DarkRed);
        }

        private void TextBoxLog_TextChanged(object sender, TextChangedEventArgs e)
        {
            var textBox = (TextBox)sender;
            if (!textBox.IsKeyboardFocusWithin)
            {
                textBox.ScrollToEnd();
            }
        }

        private void ButtonExit_Click(object sender, RoutedEventArgs e)
        {
            StopServer();
            Application.Current.Shutdown();
        }

        /* Public static methods */

        public static void AppendLog(string log, bool autoLineEnd = true)
        {
            var datePrefix = DateTime.Now.ToString("HH:mm:ss") + ": ";
            if (!log.EndsWith("\n") && autoLineEnd)
            {
                log += "\n";
            }

            Application.Current.Dispatcher.BeginInvoke(
                DispatcherPriority.Background,
                new Action(() => _logger.AppendText(datePrefix + log)));
        }

        public static void ClearLog()
        {
            Application.Current.Dispatcher.BeginInvoke(
                DispatcherPriority.Background,
                new Action(() => _logger.Clear()));
        }
    }
}
