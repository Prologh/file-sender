﻿namespace FileSender.DesktopApp.ViewModels
{
    using System.ComponentModel;

    /// <summary>
    /// Represents server view model.
    /// </summary>
    public class ServerVM : INotifyPropertyChanged
    {
        private string _ipAddress;
        private string _port;

        /// <summary>
        /// Initializes a new instance of the <see cref="ServerVM"/> class.
        /// </summary>
        public ServerVM()
        {

        }

        /// <summary>
        /// Gets or sets the IP address.
        /// </summary>
        public string IPAddress
        {
            get { return _ipAddress; }

            set
            {
                if (IPAddress != value)
                {
                    _ipAddress = value;
                    RaisePropertyChanged(nameof(FullAddress));
                    RaisePropertyChanged(nameof(IPAddress));
                }
            }
        }

        /// <summary>
        /// Gets or sets the port number.
        /// </summary>
        public string Port
        {
            get { return _port; }

            set
            {
                if (Port != value)
                {
                    _port = value;
                    RaisePropertyChanged(nameof(FullAddress));
                    RaisePropertyChanged(nameof(Port));
                }
            }
        }

        /// <summary>
        /// Gets the <see cref="string"/> representation of full end point
        /// address.
        /// </summary>
        public string FullAddress
        {
            get
            {
                return $"{IPAddress}:{Port}";
            }
        }

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        private void RaisePropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
