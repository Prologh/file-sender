﻿namespace FileSender.DesktopApp.ViewModels
{
    using System.ComponentModel;

    /// <summary>
    /// Represents end point view model.
    /// </summary>
    public class EndPointVM : INotifyPropertyChanged
    {
        private string _port;
        private string _ipAddress;

        /// <summary>
        /// Initializes a new instance of the <see cref="EndPointVM"/> class.
        /// </summary>
        public EndPointVM()
        {

        }

        /// <summary>
        /// Gets or sets the IP address.
        /// </summary>
        public string IPAddress
        {
            get { return _ipAddress; }

            set
            {
                if (IPAddress != value)
                {
                    _ipAddress = value;
                    RaisePropertyChanged(nameof(FullAddress));
                    RaisePropertyChanged(nameof(IPAddress));
                }
            }
        }

        /// <summary>
        /// Gets or sets the port number.
        /// </summary>
        public string Port
        {
            get { return _port; }

            set
            {
                if (Port != value)
                {
                    _port = value;
                    RaisePropertyChanged(nameof(FullAddress));
                    RaisePropertyChanged(nameof(Port));
                }
            }
        }

        /// <summary>
        /// Gets the <see cref="string"/> representation of full end point
        /// address.
        /// </summary>
        public string FullAddress
        {
            get
            {
                return $"{IPAddress}:{Port}";
            }
        }

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        private void RaisePropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}