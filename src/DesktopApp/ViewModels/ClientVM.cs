﻿namespace FileSender.DesktopApp.ViewModels
{
    using System.ComponentModel;

    /// <summary>
    /// Represents client view model.
    /// </summary>
    public class ClientVM : INotifyPropertyChanged
    {
        private string _fileName;
        private string _ipAddress;
        private string _port;
        private int _progress;

        /// <summary>
        /// Initializes a new instance of the <see cref="ClientVM"/> class.
        /// </summary>
        public ClientVM()
        {

        }

        /// <summary>
        /// Gets or sets the file name.
        /// </summary>
        public string FileName
        {
            get
            {
                return _fileName;
            }

            set
            {
                if (_fileName != value)
                {
                    _fileName = value;
                    RaisePropertyChanged(nameof(FileName));
                }
            }
        }

        /// <summary>
        /// Gets or sets the IP address.
        /// </summary>
        public string IPAddress
        {
            get
            {
                return _ipAddress;
            }

            set
            {
                if (_ipAddress != value)
                {
                    _ipAddress = value;
                    RaisePropertyChanged(nameof(IPAddress));
                }
            }
        }

        /// <summary>
        /// Gets or sets the port number.
        /// </summary>
        public string Port
        {
            get
            {
                return _port;
            }

            set
            {
                if (_port != value)
                {
                    _port = value;
                    RaisePropertyChanged(Port);
                }
            }
        }

        /// <summary>
        /// Gets or sets the progress value.
        /// </summary>
        public int Progress
        {
            get
            {
                return _progress;
            }

            set
            {
                if (_progress != value)
                {
                    _progress = value;
                    RaisePropertyChanged(nameof(Progress));
                }
            }
        }

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        private void RaisePropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
